package com.mx.movies

import com.mx.movies.data.dataSource.MovieRemoteDataSource
import com.mx.movies.data.dataSource.MovieRepositoryImpl
import com.mx.movies.data.dataSource.remote.MovieApiService
import com.mx.movies.data.dataSource.remote.MovieRemoteDataSourceImpl
import com.mx.movies.domain.MovieRepository
import com.mx.movies.domain.useCase.getMovies.GetMoviesUseCase
import com.mx.movies.presentation.getMovies.GetMovies
import com.mx.movies.presentation.getMovies.GetMoviesImpl
import org.koin.dsl.module
import retrofit2.Retrofit

val movieModule = module {

    /** PRESENTATION */
    single<GetMovies> { GetMoviesImpl(getMoviesUseCase = get()) }

    /** USE CASE */
    factory { GetMoviesUseCase(repository = get()) }

    /** REPOSITORY */
    single<MovieRepository> {
        MovieRepositoryImpl(
            movieRemoteDataSource = get(),
            internetConnectionRepository = get()
        )
    }

    /** DATA SOURCE REMOTE */
    single<MovieRemoteDataSource> {
        MovieRemoteDataSourceImpl(
            apiService = get()
        )
    }

    /** API SOURCE */
    single { get<Retrofit>().create(MovieApiService::class.java) }

}