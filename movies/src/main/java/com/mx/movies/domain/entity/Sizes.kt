package com.mx.movies.domain.entity

data class Sizes(
    val largeSize: String?,
    val medium: String?,
    val small: String?,
    val xLarge: String?
)