package com.mx.movies.domain.useCase.getMovies

import com.mx.domain.Either
import com.mx.domain.UseCase
import com.mx.movies.domain.MovieRepository

class GetMoviesUseCase(
    private val repository: MovieRepository
): UseCase<GetMoviesResponse, GetMoviesParams, GetMoviesFailure>() {

    /** */
    override suspend fun run(
        params: GetMoviesParams
    ): Either<GetMoviesFailure, GetMoviesResponse> =
        repository.getMovies(params)


}