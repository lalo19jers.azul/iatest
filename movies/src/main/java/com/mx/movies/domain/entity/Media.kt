package com.mx.movies.domain.entity

data class Media(
    val resource: String,
    val type: String,
    val code: String
)
