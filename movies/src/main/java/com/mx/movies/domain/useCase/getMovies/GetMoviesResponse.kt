package com.mx.movies.domain.useCase.getMovies

import com.mx.movies.domain.entity.Movie
import com.mx.movies.domain.entity.Route

data class GetMoviesResponse(
    val movies: List<Movie>,
    val routes: List<Route>
)