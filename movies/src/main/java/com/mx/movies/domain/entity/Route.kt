package com.mx.movies.domain.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class Route(
    val code: String,
    val sizes: @RawValue Sizes
): Parcelable
