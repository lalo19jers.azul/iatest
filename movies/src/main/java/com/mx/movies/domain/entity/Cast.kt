package com.mx.movies.domain.entity

data class Cast(
    val label: String,
    val castList: List<String>
)
