package com.mx.movies.domain

import com.mx.domain.Either
import com.mx.movies.domain.useCase.getMovies.GetMoviesFailure
import com.mx.movies.domain.useCase.getMovies.GetMoviesParams
import com.mx.movies.domain.useCase.getMovies.GetMoviesResponse

interface MovieRepository {

    /** */
    suspend fun getMovies(
        params: GetMoviesParams
    ): Either<GetMoviesFailure, GetMoviesResponse>
}