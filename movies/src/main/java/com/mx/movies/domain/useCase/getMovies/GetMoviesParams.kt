package com.mx.movies.domain.useCase.getMovies

data class GetMoviesParams(
    val countryCode: String,
    val cinema: String
)