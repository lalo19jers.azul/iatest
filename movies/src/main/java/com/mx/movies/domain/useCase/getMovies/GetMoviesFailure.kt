package com.mx.movies.domain.useCase.getMovies

import com.mx.datasource.failureMessage.HttpFailure
import com.mx.domain.Failure

sealed class GetMoviesFailure : Failure.FeatureFailure() {
    /* */
    object NetworkConnectionFailure : GetMoviesFailure()

    /* */
    object NoMoviesFound : GetMoviesFailure()

    /* */
    object NullPointerException: GetMoviesFailure()

    /* */
    data class JsonDataDeserializationFailure(val message: String?) : GetMoviesFailure()

    /* */
    data class ServerFailure(
        override val code: Int,
        override val message: String
    ) : GetMoviesFailure(), HttpFailure

    /* */
    data class UnknownFailure(val message: String?) : GetMoviesFailure()

    /** */
    companion object {
        internal fun fromFeatureFailure(failure: Failure) = when (failure) {
            is GetMoviesFailure -> failure
            else -> UnknownFailure(failure.javaClass.simpleName)
        }
    }
}