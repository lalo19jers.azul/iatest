package com.mx.movies.domain.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class Movie(
    val rating: String,
    val media: @RawValue List<Media>,
    val cast: @RawValue List<Cast>,
    val genre: String,
    val synopsis: String,
    val length: String,
    val id: Int,
    val name: String,
    val code: String,
    val originalName: String
): Parcelable
