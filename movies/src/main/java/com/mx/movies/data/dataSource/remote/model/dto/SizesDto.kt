package com.mx.movies.data.dataSource.remote.model.dto

import com.mx.movies.domain.entity.Sizes
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SizesDto(
    @Json(name = "large") val largeSize: String?,
    @Json(name = "medium") val medium: String?,
    @Json(name = "small") val small: String?,
    @Json(name = "x-large") val xLarge: String?
) {

    /** */
    fun toSizes(): Sizes =
        Sizes(largeSize, medium, small, xLarge)
}