package com.mx.movies.data.dataSource.remote

import com.mx.datasource.remote.model.retrofitApiCall
import com.mx.domain.Either
import com.mx.movies.data.dataSource.MovieRemoteDataSource
import com.mx.movies.data.dataSource.remote.failure.toMovieFailure
import com.mx.movies.domain.useCase.getMovies.GetMoviesFailure
import com.mx.movies.domain.useCase.getMovies.GetMoviesParams
import com.mx.movies.domain.useCase.getMovies.GetMoviesResponse
import retrofit2.HttpException
import java.lang.Exception

internal class MovieRemoteDataSourceImpl(
    private val apiService: MovieApiService
) : MovieRemoteDataSource {

    /** */
    override suspend fun getMovies(
        params: GetMoviesParams
    ): Either<GetMoviesFailure, GetMoviesResponse> =
        try {
            retrofitApiCall {
                apiService.getMovies(
                    countryCode = params.countryCode,
                    cinema = params.cinema
                )
            }.let { response ->
                Either.Right(GetMoviesResponse(
                    movies = response.movies.map { it.toMovie() },
                    routes = response.routes.map { it.toRoute() }
                ))
            }
        } catch (e: Exception) {
            val failure = when (e) {
                is HttpException -> e.toMovieFailure()
                else -> GetMoviesFailure.UnknownFailure(e.message)
            }
            Either.Left(failure)
        }
}