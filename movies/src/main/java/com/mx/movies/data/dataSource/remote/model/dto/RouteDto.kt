package com.mx.movies.data.dataSource.remote.model.dto

import com.mx.movies.domain.entity.Route
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RouteDto(
    @Json(name = "code") val code: String,
    @Json(name = "sizes") val sizes: SizesDto


) {

    /** */
    fun toRoute(): Route = Route(code, sizes.toSizes())

}
