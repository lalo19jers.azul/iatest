package com.mx.movies.data.dataSource.remote.model.response

import com.mx.movies.data.dataSource.remote.model.dto.MovieDto
import com.mx.movies.data.dataSource.remote.model.dto.RouteDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MoviesResponse(
    @Json(name = "movies") val movies: List<MovieDto>,
    @Json(name = "routes") val routes: List<RouteDto>
)
