package com.mx.movies.data.dataSource.remote.model.dto

import com.mx.movies.domain.entity.Cast
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CastDto(
    @Json(name = "label") val label: String,
    @Json(name = "value") val castList: List<String>
) {

    /** */
    fun toCast() : Cast =
        Cast(
            label = label,
            castList = castList
        )
}