package com.mx.movies.data.dataSource

import com.mx.domain.Either
import com.mx.movies.domain.useCase.getMovies.GetMoviesFailure
import com.mx.movies.domain.useCase.getMovies.GetMoviesParams
import com.mx.movies.domain.useCase.getMovies.GetMoviesResponse


internal interface MovieRemoteDataSource {

    /** */
    suspend fun getMovies(
        params: GetMoviesParams
    ): Either<GetMoviesFailure, GetMoviesResponse>
}