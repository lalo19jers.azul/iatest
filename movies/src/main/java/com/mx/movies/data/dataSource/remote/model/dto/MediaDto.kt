package com.mx.movies.data.dataSource.remote.model.dto

import com.mx.movies.domain.entity.Media
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MediaDto(
    @Json(name = "resource") val resource: String,
    @Json(name = "type") val type: String,
    @Json(name = "code") val code: String
) {

    /** */
    fun toMedia() : Media =
        Media(
            resource = resource,
            type = type,
            code = code
        )
}