package com.mx.movies.data.dataSource

import com.mx.domain.Either
import com.mx.movies.domain.MovieRepository
import com.mx.movies.domain.useCase.getMovies.GetMoviesFailure
import com.mx.movies.domain.useCase.getMovies.GetMoviesParams
import com.mx.movies.domain.useCase.getMovies.GetMoviesResponse
import com.mx.network.internetConnection.InternetConnectionRepository

internal class MovieRepositoryImpl(
    private val movieRemoteDataSource: MovieRemoteDataSource,
    internetConnectionRepository: InternetConnectionRepository
) : MovieRepository, InternetConnectionRepository by internetConnectionRepository {

    /*
    How it should be works:
    if (isOnline)
            eventRemoteDataSource.getEvents()
    else eventLocalDataSource.getEvents()
     */

    override suspend fun getMovies(
        params: GetMoviesParams
    ): Either<GetMoviesFailure, GetMoviesResponse> =
        movieRemoteDataSource.getMovies(params)
    /**
     * NOTE: for some reason variable [isOnline] always returns false.
     * I don't have more time to check and fix this issue but code commented above
     * is an example that how [isOnline] help us to verify is we connect to remoteDataSource
     * or localDataSource.
     * And this way I decided not change [GetEventsResponse] from List<EventItem>(model entity) to
     * List<Event> (db entity)
     */

}