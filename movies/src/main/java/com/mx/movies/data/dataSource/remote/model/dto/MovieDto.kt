package com.mx.movies.data.dataSource.remote.model.dto

import com.mx.movies.domain.entity.Movie
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MovieDto(
    @Json(name = "rating") val rating: String,
    @Json(name = "media") val media: List<MediaDto>,
    @Json(name = "cast") val cast: List<CastDto>,
    @Json(name = "genre") val genre: String,
    @Json(name = "synopsis") val synopsis: String,
    @Json(name = "length") val length: String,
    @Json(name = "id") val id: Int,
    @Json(name = "name") val name: String,
    @Json(name = "code") val code: String,
    @Json(name = "original_name") val originalName: String


) {

    /** */
    fun toMovie(): Movie =
       Movie(
           rating = rating,
           media = media.map { it.toMedia() },
           cast = cast.map { it.toCast() },
           genre = genre,
           synopsis = synopsis,
           length = length,
           id = id,
           name = name,
           code = code,
           originalName = originalName
       )

}
