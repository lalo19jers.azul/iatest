package com.mx.movies.data.dataSource.remote.failure

import com.mx.datasource.remote.model.HttpErrorCode
import com.mx.movies.domain.useCase.getMovies.GetMoviesFailure
import retrofit2.HttpException

/** */
internal fun HttpException.toMovieFailure(): GetMoviesFailure =
    when(HttpErrorCode.from(code())) {
        HttpErrorCode.HTTP_NOT_FOUND -> GetMoviesFailure.NoMoviesFound
        else -> GetMoviesFailure.ServerFailure(code(),message())
    }