package com.mx.movies.data.dataSource.remote

import com.mx.movies.data.dataSource.remote.model.response.MoviesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

internal interface MovieApiService {

    /** */
    @GET(URL.GET_MOVIES)
    suspend fun getMovies(
        @Query("country_code") countryCode: String,
        @Query("cinema") cinema: String,
    ): Response<MoviesResponse>

    private object URL {
        const val GET_MOVIES = "/v2/movies"
    }
}