package com.mx.movies.presentation.getMovies

import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import com.mx.domain.Status
import com.mx.domain.onFailure
import com.mx.domain.onRight
import com.mx.movies.domain.useCase.getMovies.GetMoviesParams
import com.mx.movies.domain.useCase.getMovies.GetMoviesUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow

class GetMoviesImpl(
    private val getMoviesUseCase: GetMoviesUseCase
) : GetMovies {


    /** */
    override fun getMoviesAsLiveData(
        params: GetMoviesParams
    ): LiveData<GetMoviesStatus> = flow<GetMoviesStatus> {
        emit(Status.Loading())
        getMoviesUseCase.run(params)
            .onFailure { emit(Status.Error(it)) }
            .onRight {  emit(Status.Done(it))  }
    }.asLiveData(Dispatchers.IO)

}