package com.mx.movies.presentation.getMovies

import androidx.lifecycle.LiveData
import com.mx.domain.Status
import com.mx.movies.domain.useCase.getMovies.GetMoviesFailure
import com.mx.movies.domain.useCase.getMovies.GetMoviesParams
import com.mx.movies.domain.useCase.getMovies.GetMoviesResponse


/** */
typealias GetMoviesStatus =
        Status<GetMoviesFailure, GetMoviesResponse>

interface GetMovies {

    /** */
    fun getMoviesAsLiveData(
        params: GetMoviesParams
    ): LiveData<GetMoviesStatus>

}