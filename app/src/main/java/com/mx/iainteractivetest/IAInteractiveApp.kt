package com.mx.iainteractivetest

import android.app.Application
import com.mx.iainteractivetest.di.initKoin
import timber.log.Timber

class IAInteractiveApp: Application() {

    /** */
    override fun onCreate() {
        super.onCreate()
        initKoin()
        Timber.plant(Timber.DebugTree())
    }

}