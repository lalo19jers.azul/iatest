package com.mx.iainteractivetest.presentation.movies.getMovies.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.mx.iainteractivetest.databinding.ItemMovieBinding
import com.mx.iainteractivetest.presentation.common.extensions.loadImage
import com.mx.movies.domain.entity.Movie
import com.mx.movies.domain.entity.Route

class MovieViewHolder(
    private val binding: ItemMovieBinding
) : RecyclerView.ViewHolder(binding.root) {


    /** */
    fun bind(movie: Movie, routes: List<Route>, onRowClick: (Movie) -> Unit) {
        binding.run {
            ivBackground.loadImage(routes.find { it.code == POSTER }?.sizes?.largeSize.plus(
                movie.media.find { it.code == POSTER }?.resource
            ))
            root.setOnClickListener { onRowClick(movie) }
        }
    }

    /**
     * Inflates item
     */
    companion object {

        const val POSTER = "poster"

        fun from(parent: ViewGroup): MovieViewHolder {
            val layoutInflater = ItemMovieBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
            return MovieViewHolder(layoutInflater)
        }
    }
}