package com.mx.iainteractivetest.presentation.movies.getMovies

import androidx.lifecycle.ViewModel
import com.mx.movies.presentation.getMovies.GetMovies

class GetMoviesViewModel(
    getMovies: GetMovies
) : ViewModel(), GetMovies by getMovies