package com.mx.iainteractivetest.presentation.movies.getMovies.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.mx.movies.domain.entity.Movie
import com.mx.movies.domain.entity.Route

class MovieAdapter(
    private val routes: List<Route>,
    private val onRowClick: (Movie) -> Unit
) : ListAdapter<Movie, MovieViewHolder>(MovieDiffUtil()) {

    /** */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder =
        MovieViewHolder.from(parent)

    /** */
    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = getItem(position)
        holder.bind(movie = movie, onRowClick = onRowClick, routes = routes)
    }

    /** */
    internal class MovieDiffUtil : DiffUtil.ItemCallback<Movie>() {
        /** */
        override fun areItemsTheSame(
            oldItem: Movie,
            newItem: Movie
        ): Boolean = oldItem == newItem

        /** */
        override fun areContentsTheSame(
            oldItem: Movie,
            newItem: Movie
        ): Boolean = oldItem == newItem

    }
}