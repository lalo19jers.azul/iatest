package com.mx.iainteractivetest.presentation.movies.getMovies

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mx.domain.Status
import com.mx.iainteractivetest.databinding.GetMoviesFragmentBinding
import com.mx.iainteractivetest.presentation.common.extensions.hideProgressBar
import com.mx.iainteractivetest.presentation.common.extensions.presentShortSnackBar
import com.mx.iainteractivetest.presentation.common.extensions.showProgressBar
import com.mx.iainteractivetest.presentation.movies.getMovies.adapter.MovieAdapter
import com.mx.movies.domain.entity.Movie
import com.mx.movies.domain.entity.Route
import com.mx.movies.domain.useCase.getMovies.GetMoviesParams
import com.mx.movies.domain.useCase.getMovies.GetMoviesResponse
import com.mx.movies.presentation.getMovies.GetMoviesStatus
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class GetMoviesFragment : Fragment() {

    /* */
    private val binding: GetMoviesFragmentBinding
            by lazy { GetMoviesFragmentBinding.inflate(layoutInflater) }

    /* */
    private val viewModel: GetMoviesViewModel by viewModel()

    /* */
    private lateinit var movieAdapter: MovieAdapter

    /* */
    private var routes = emptyArray<Route>()



    /** */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    /** */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getMovies()
    }

    /** */
    private fun getMovies() {
        viewModel.getMoviesAsLiveData(
            GetMoviesParams(countryCode = "MX", cinema = "61")
        ).observe(
            viewLifecycleOwner,
            getMoviesStatusObserver()
        )
    }

    /** */
    private fun getMoviesStatusObserver() = Observer<GetMoviesStatus> {
        requireContext().hideProgressBar()
        when (it) {
            is Status.Loading -> requireContext().showProgressBar()
            is Status.Error -> {
                Timber.i("ERROR -> ${it.failure}")
                binding.root.presentShortSnackBar(it.failure.toString())
            }
            is Status.Done -> setUpView(it.value)
        }
    }

    /** */
    private fun setUpView(moviesResponse: GetMoviesResponse) {
        binding.apply {
            routes = moviesResponse.routes.toTypedArray()
            movieAdapter = MovieAdapter(routes = moviesResponse.routes, onRowClick = onRowClick)
            movieAdapter.submitList(moviesResponse.movies)
            rvMovies.adapter = movieAdapter
        }
    }

    /** */
    private val onRowClick: (Movie) -> Unit = {
        val directions = GetMoviesFragmentDirections.actionGetMoviesFragmentToDetailMovieFragment(
            movie = it,
            route = routes
        )
        findNavController().navigate(directions)
    }


}