package com.mx.iainteractivetest.presentation.movies.trailer

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.webkit.URLUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Renderer
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.mx.iainteractivetest.databinding.TrailerMovieFragmentBinding
import java.io.File

class TrailerMovieFragment : Fragment() {

    /* */
    private val binding: TrailerMovieFragmentBinding
            by lazy { TrailerMovieFragmentBinding.inflate(layoutInflater) }

    /* */
    private var videoPlayer: SimpleExoPlayer? = null

    /* */
    private val args: TrailerMovieFragmentArgs by navArgs()

    /** */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    /** */
    override fun onResume() {
        super.onResume()
        releasePlayer()
        mediaPlayerConfig()
    }

    /** */
    override fun onStop() {
        super.onStop()
        releasePlayer()
    }

    /** */
    override fun onPause() {
        super.onPause()
        releasePlayer()
    }

    /**
     *
     */
    @SuppressLint("UseCompatLoadingForDrawables")
    private fun mediaPlayerConfig() {
        videoPlayer = SimpleExoPlayer.Builder(requireContext()).build()

        // Creates video playlist with videos given by server

        val filenameVideo = URLUtil.guessFileName(
            args.videoUrl,
            null,
            MimeTypeMap.getFileExtensionFromUrl(args.videoUrl)
        )
        val file = File(context?.getExternalFilesDir(Environment.DIRECTORY_MOVIES), filenameVideo)
        val mediaItem = if (file.exists()) {
            val localUri = Uri.fromFile(file)
            MediaItem.fromUri(localUri)
        } else MediaItem.fromUri(Uri.parse(args.videoUrl))

        videoPlayer?.setMediaItem(mediaItem)

        binding.videosFragmentVideoView.apply {
            player = videoPlayer
            useArtwork = true
            resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIT
        }

        videoPlayer?.apply {
            seekTo(0, 0)
            playWhenReady = true
            videoScalingMode = Renderer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING
            prepare()
        }

    }

    /** */
    private fun releasePlayer() {
        if (videoPlayer != null) {
            videoPlayer?.release()
            videoPlayer = null
        }
    }

}