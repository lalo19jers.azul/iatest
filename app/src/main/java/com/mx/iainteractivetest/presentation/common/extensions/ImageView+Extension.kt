/*
 * ImageView+Extension.kt
 * PhunApp
 *
 * Created by lalo on 14/5/21 01:24
 * Copyright (c) 2021 . All rights reserved.
 */

package com.mx.iainteractivetest.presentation.common.extensions

import android.widget.ImageView
import coil.api.load
import coil.size.Scale
import com.mx.iainteractivetest.R

/** */
fun ImageView.loadImage(url: String?) {
    url?.let {
        if (it.isValidUrl())
            this.load(url) {
                error(R.drawable.movie_bg)
                scale(Scale.FIT)
            }
        else load(R.drawable.movie_bg)
    }

}
