package com.mx.iainteractivetest.presentation.movies.detailMovie

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.mx.iainteractivetest.R
import com.mx.iainteractivetest.databinding.DetailMovieFragmentBinding
import com.mx.iainteractivetest.presentation.common.extensions.loadImage
import com.mx.movies.domain.entity.Movie
import com.mx.movies.domain.entity.Route
import kotlinx.android.synthetic.main.detail_movie_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailMovieFragment : Fragment() {

    /* */
    private val binding: DetailMovieFragmentBinding
            by lazy { DetailMovieFragmentBinding.inflate(layoutInflater) }

    /* */
    private val viewModel: DetailMovieViewModel by viewModel()

    /* */
    private val args: DetailMovieFragmentArgs by navArgs()


    /** */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    /** */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showMovieDetail(args.movie, args.route.toList())
    }

    /** */
    private fun showMovieDetail(movie: Movie, routes: List<Route>) {
        binding.apply {
            ivMoviePoster.loadImage(routes.find { it.code == POSTER_HOR }?.sizes?.largeSize.plus(
                movie.media.find { it.code == POSTER_HOR }?.resource
            ))
            tvMovieTitle.text = movie.name
            tvMovieDuration.text = getString(R.string.duration,movie.length)
            tvMovieCategory.text = getString(R.string.category,movie.genre)
            tvMovieSynopsis.text = getString(R.string.synopsis,movie.synopsis)

            ivViewTrailer.setOnClickListener {
                viewMovieTrailer(
                    videoUrl =
                    routes.find { it.code == TRAILER }?.sizes?.medium.plus(
                        movie.media.find { it.code == TRAILER }?.resource)
                )
            }
            tvViewTrailer.setOnClickListener {
                viewMovieTrailer(
                    videoUrl =
                    routes.find { it.code == TRAILER }?.sizes?.medium.plus(
                        movie.media.find { it.code == TRAILER }?.resource)
                )
            }

        }
    }

    /** */
    private fun viewMovieTrailer(videoUrl: String) {
        val directions = DetailMovieFragmentDirections.actionDetailMovieFragmentToTrailerMovieFragment(
            videoUrl = videoUrl
        )

        findNavController().navigate(directions)
    }

    companion object {
        const val POSTER_HOR = "poster_horizontal"
        const val TRAILER = "trailer_mp4"
    }

}