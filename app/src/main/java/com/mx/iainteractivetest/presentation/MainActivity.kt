package com.mx.iainteractivetest.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.mx.iainteractivetest.R
import com.mx.iainteractivetest.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    /* */
    private val binding: ActivityMainBinding
            by lazy { ActivityMainBinding.inflate(layoutInflater) }

    /* */
    private lateinit var navController: NavController

    /** */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        navController = findNavController(R.id.activity_main_navHost)
        setupToolbar()
    }

    /** */
    private fun setupToolbar() {
        binding.toolbar.setupWithNavController(navController)
    }
}