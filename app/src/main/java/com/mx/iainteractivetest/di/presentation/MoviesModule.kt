package com.mx.iainteractivetest.di.presentation

import com.mx.iainteractivetest.presentation.movies.getMovies.GetMoviesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/* */
val moviesModule = module {
    viewModel { GetMoviesViewModel(getMovies = get()) }
}