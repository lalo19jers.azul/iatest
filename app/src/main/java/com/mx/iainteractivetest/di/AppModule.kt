package com.mx.iainteractivetest.di

import com.mx.datasource.httpClientModule
import com.mx.iainteractivetest.IAInteractiveApp
import com.mx.iainteractivetest.di.presentation.moviesModule
import com.mx.movies.movieModule
import com.mx.network.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import org.koin.core.logger.Level

/**
 * This class is our dependency injection. Built with koin.
 * Our App architecture is Clean Architecture + MVVM.
 * And we change the Viewmodel factory with this dependency injection.
 * If you need more parameters will be welcome in this module.
 */
fun IAInteractiveApp.initKoin() {
    startKoin {
        val modules = getPresentationModules() + getSharedModules() + getFeatureModules()
        androidLogger(Level.ERROR)
        androidContext(applicationContext)
        modules(modules)
    }
}

/**
 *
 * @return [List]
 */
private fun getSharedModules(): List<Module> = listOf(
    /** **/
    networkModule,
    httpClientModule

)

/**
 *
 * @return [List]
 */
private fun getFeatureModules(): List<Module> = listOf(
    movieModule
)

/**
 * Presentation module is the layer that interacts with UI.
 * Presentation layer contains ViewModel, Fragments and Activities.
 * @return [List]
 */
private fun getPresentationModules(): List<Module> = listOf(
    /**  **/
    moviesModule

)