package com.mx.datasource.remote.httpClient

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitBuilder(
    private val baseUrl: String,
    private val apiKeyValue: String,
    private val headerInterceptor: HeaderInterceptor? = null
) {

    /**
     *  Logging interceptor. By default, Retrofit doesn't log any request
     */
    private val loggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    /** */
    private val moshiConverterFactory = MoshiConverterFactory.create()

    /** */
    private val timeOut = 100L

    /**
     * Build Retrofit instance
     * @return [Retrofit]
     */
    fun build(): Retrofit =
        Retrofit.Builder().client(buildHttpClient())
            .baseUrl(baseUrl)
            .addConverterFactory(moshiConverterFactory)
            .build()

    /**
     * @return [OkHttpClient]
     */
    private fun buildHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(timeOut, TimeUnit.SECONDS)
            .readTimeout(timeOut, TimeUnit.SECONDS)
            .addInterceptor(getHeaderInterceptor())
            .apply { addInterceptor(loggingInterceptor) }
            .build()

    /**
     * @return [Interceptor]
     */
    private fun getHeaderInterceptor() = Interceptor {
        val authorizationType = headerInterceptor?.getAuthorizationType()
        val accessToken = headerInterceptor?.getAuthorizationValue()
        if (accessToken == null) {
            val request = it.request().newBuilder()
                .addHeader(Constants.API_KEY_HEADER, apiKeyValue)
                .build()
            it.proceed(request)
        } else {
            val newRequest = it.request().newBuilder()
                .addHeader(Constants.HEADER_VALUE, "$authorizationType $accessToken")
                .addHeader(Constants.API_KEY_HEADER, apiKeyValue)
                .build()
            it.proceed(newRequest)
        }
    }

    /* */
    private object Constants {

        /* */
        const val HEADER_VALUE = "Authorization"

        /* */
        const val API_KEY_HEADER = "api_key"
    }


}