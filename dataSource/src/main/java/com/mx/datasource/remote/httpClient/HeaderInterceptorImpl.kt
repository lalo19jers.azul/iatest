package com.mx.datasource.remote.httpClient

class HeaderInterceptorImpl: HeaderInterceptor {

    /**
     * @return [String]
     */
    override fun getAuthorizationType(): String = "JWT"

    /**
     * @return [String]
     */
    override fun getAuthorizationValue(): String? {
        return null
    }

}