/*
 * DefaultFailure.kt
 * PhunApp
 *
 * Created by lalo on 13/5/21 01:31
 * Copyright (c) 2021 . All rights reserved.
 */

package com.mx.datasource.failureMessage

/** */
interface DefaultFailure {

    /** */
    val message: String

}