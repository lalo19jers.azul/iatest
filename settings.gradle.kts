include(":app")
include(":dataSource")
include(":domain")
include(":network")
include(":movies")

rootProject.name = ("IAInteractiveTest")

